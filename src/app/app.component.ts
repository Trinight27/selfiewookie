import { Component } from '@angular/core';
import { Selfie } from './models/selfie';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Selfie à Wookie';
  logoAffiche = true;
  lesSelfies : Selfie[] = [
    { image : '', wookie : {nom : 'Chico', selfies : []}},
    { image : '', wookie : {nom : 'Chewy', selfies : []}}
  ];
}
